
bin/prueba: obj/prueba.o obj/Ejemplo.o obj/slaballoc.o obj/carro.o obj/persona.o obj/estudiante.o
	gcc -g obj/prueba.o obj/Ejemplo.o obj/slaballoc.o obj/carro.o obj/persona.o obj/estudiante.o -o bin/prueba

obj/prueba.o: src/prueba.c
	gcc -g -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -g -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o

obj/slaballoc.o: src/slaballoc.c
	gcc -g -Wall -c -Iinclude/ src/slaballoc.c -o obj/slaballoc.o

obj/carro.o: src/carro.c
	gcc -g -Wall -c -Iinclude/ src/carro.c -o obj/carro.o

obj/persona.o: src/persona.c
	gcc -g -Wall -c -Iinclude/ src/persona.c -o obj/persona.o

obj/estudiante.o: src/estudiante.c
	gcc -g -Wall -c -Iinclude/ src/estudiante.c -o obj/estudiante.o
#agregue las reglas que necesite


.PHONY: clean
clean:
	rm bin/* obj/*.o

.PHONY: run
run: bin/prueba
	bin/prueba
