#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_carro(void *ref, size_t tamano){
    Carro *car = (Carro *)ref;
    car->anio = 2019;
    car->kilometraje = 4351200;
}

void destruir_carro(void *ref, size_t tamano){
    Carro *car = (Carro *)ref;
    car->anio = -1;
    car->kilometraje = -1;
}
