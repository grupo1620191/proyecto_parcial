#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_persona(void *ref, size_t tamano){
    Persona *pers = (Persona *)ref;
    pers->edad = 22;
    pers->nombre = (char *)malloc(sizeof(char)*100);
}

void destruir_persona(void *ref, size_t tamano){
    Persona *pers = (Persona *)ref;
    pers->edad = -1;
    free(pers->nombre);
}
