#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"


void crear_stdnt(void *ref, size_t tamano){
    Estudiante *stdnt = (Estudiante *)ref;
    stdnt->carrera = "Ingenieria en Ciencias Computacionales";
    int i = 0;
    for(i = 0; i< 100; i++){
    	stdnt->notas[i] = i;
    }
}

void destruir_stdnt(void *ref, size_t tamano){
    Estudiante *stdnt = (Estudiante *)ref;
    stdnt->carrera = NULL;
    int i = 0;
    for(i = 0; i< 100; i++){
    	stdnt->notas[i] = 0;
    }
}
