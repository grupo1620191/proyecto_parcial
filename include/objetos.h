//En este archivo ud podra definir los objetos que podrán ser usados
//con el slab allocator. Declare tambien el constructor y destructor de
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo;

//Constructor
void crear_Ejemplo(void *ref, size_t tamano);

//Destructor
void destruir_Ejemplo(void *ref, size_t tamano);

//TODO: Crear 3 objetos mas

typedef struct persona{
    int edad;
    char *nombre;
} Persona;

//Constructor
void crear_persona(void *ref, size_t tamano);

//Destructor
void destruir_persona(void *ref, size_t tamano);

typedef struct carro {
    int anio;
    long kilometraje;
} Carro;

//Constructor
void crear_carro(void *ref, size_t tamano);

//Destructor
void destruir_carro(void *ref, size_t tamano);

typedef struct estudiante{
    float notas[100];
    char *carrera;
} Estudiante;

//Constructor
void crear_estudiante(void *ref, size_t tamano);

//Destructor
void destruir_estudiante(void *ref, size_t tamano);
